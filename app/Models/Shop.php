<?php

namespace App\Models;

use App\Models\User;
use App\Models\Icecream;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Scout\Searchable;

class Shop extends Model
{
    use Searchable;
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'address',
        'img'
    ];

    public function toSearchableArray()
    {
        $icecreams=$this->icecreams->pluck('name','brand','description')->join(', ');
        $array = [
            
            'id'=> $this->id,
            'name'=> $this->name,
            'description'=> $this->description,
            'address'=> $this->address,
            'icecreams'=>$icecreams,
        ]; 

        

        return $array;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function icecreams()
    {
        return $this->belongsToMany(Icecream::class);
    }
}
