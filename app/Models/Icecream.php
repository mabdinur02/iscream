<?php

namespace App\Models;

use App\Models\Shop;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Flavour;

class Icecream extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'brand',
        'description',
        'img'
    ];

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

}
