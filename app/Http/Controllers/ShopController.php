<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Models\Icecream;
use Illuminate\Http\Request;
use App\Http\Requests\ShopRequest;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        $shops = Shop::all();

        return view('home',compact('shops'));
    }

    public function search(Request $request) {
        
        $q= $request->input('q');

        $shops= Shop::search('q')->get();

        return view('shop.search_results',compact('q','shops'));
    }
   
    public function create()
    {
        $icecreams = Icecream::all();
        return view('shop.create', compact('icecreams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopRequest $request)
    {
        if ($request->img) {
            $shop = Shop::create([
                'name'=> $request->name,
                'description' => $request->description,
                'address' => $request->address,
                'img' => $request->file('img')->store('public/img')
                ]); 
            } else {
                $shop = Shop::create([
                    'name'=> $request->name,
                    'description' => $request->description,
                    'address' => $request->address,
                    ]); 
                }
  
            foreach ($request->selection as $icecream){
                $shop->icecreams()->attach($icecream);
            }
          
                
       
      
       return redirect(route('thankyou'));
    }


    public function thankyou() {
        return view('thankyou');
    }

   
    public function show(Shop $shop)
    { 
       return view('shop.show',compact('shop'));
    }

    
    public function edit(Shop $shop)
    {
        return view('shop.edit',compact('shop'));
    }

  
    public function update(Request $request, Shop $shop)
    {
        if ($request->img) {
            $shop->name = $request->input('name');
            $shop->descriiption = $request->input('description');
            $shop->img = $request->file('img')->store('public/img');
            $shop->save();
        } else {
            $shop->name = $request->input('name');
            $shop->description = $request->input('description');
            $shop->save();
        }

        
        foreach ($request->selection as $icecream){
            $shop->icecreams()->attach($icecream);
        }
      

        return redirect(route('home'))->with('status','Your shop was updated');
    }

    
    public function destroy(Shop $shop)
    {
        $shop->delete();

        return redirect(route('home'))->with('status','Your shop was deleted');
    }
}
