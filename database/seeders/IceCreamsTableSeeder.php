<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IceCreamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $icecreams= [
            ['name'=>'Netflix & chill','brand'=>'Ben & Jerrys','description'=>'Peanut Butter Ice Cream with Sweet & Salty Pretzel Swirls & Fudge Brownies','img'=>'\img\flavours\1_bj.png'],
             ['name'=>'Chip Happens','brand'=>'Ben & Jerrys','description' =>'A Cold Mess of Chocolate Ice Cream with Fudge Chips & Crunchy Potato Chip Swirls','img'=>'\img\flavours\2_bj.png'],
             ['name'=>'Cannoli','brand'=>'Ben & Jerrys', 'description'=>'Mascarpone Ice Cream with Fudge-Covered Pastry Shell Pieces & Mascarpone Swirls','img'=>'\img\flavours\3_bj.png'],
             ['name'=>'Natural Vanilla','brand'=>'Breyers', 'description'=>'Natural Vanilla is made with fresh cream, sugar, milk, and Rainforest Alliance Certified vanilla beans.','img'=>'\img\flavours\0_breyers.png'],
             ['name'=>'Homemade Vanilla','brand'=>'Breyers', 'description'=>'Homemade Vanilla ice cream is thick and smooth like it was just churned.','img'=>'\img\flavours\1_breyers.png'],
             ['name'=>'Extra Creamy Vanilla','brand'=>'Breyers', 'description'=>'Extra Creamy Vanilla is our creamiest vanilla. Fresh cream and sweet vanilla come together to make your favorite treat creamier than ever before! ','img'=>'\img\flavours\2_breyers.png'],
             ['name'=>'BANANA CARAMEL CRUNCH','brand'=>'Talenti', 'description'=>'This flavor was inspired by a classic southern dessert: banana pudding.','img'=>'\img\flavours\1_talenti.png'],
             ['name'=>'BELGIAN CHOCOLATE GELATO','brand'=>'Talenti', 'description'=>'Our Belgian Chocolate gelato is made with melted, imported Belgian chocolate blended until smooth with fresh milk, cream, eggs and a hint of vanilla. ','img'=>'\img\flavours\2_talenti.png'],
             ['name'=>'BLACK RASPBERRY CHOCOLATE CHIP GELATO','brand'=>'Talenti', 'description'=>',"Tart and sweet, our Black Raspberry Chocolate Chip gelato combines black raspberries. ','img'=>'\img\flavours\3_talenti.png'],
             ['name'=>'Caramel Soft Dipped Ice Cream Bar','brand'=>'Hagen Dasz', 'description'=>'A delicious pairing of indulgent caramel ice cream with a rich caramel swirl, covered in a soft chocolate truffle coating.','img'=>'\img\flavours\5_hd.png'],
             ['name'=>'Caramel Cone Ice Cream Bar','brand'=>'Hagen Dasz', 'description'=>'Golden ribbons of caramel in creamy vanilla gelato, enrobed in rich milk chocolate and crunchy italian style cone pieces.','img'=>'\img\flavours\7_hd.png'],
             ['name'=>'Chocolate Soft Dipped Ice Cream Bar','brand'=>'Hagen Dasz', 'description'=>'A decadent pairing of rich chocolate ice cream covered in a soft chocolate truffle coating.','img'=>'\img\flavours\10_hd.png'],
            

        ];

        foreach ($icecreams as $icecream){
            DB::table('icecreams')->insert([
                'name' =>$icecream['name'],
                'brand'=>$icecream['brand'],
                'description' =>$icecream['description'],
                'img'=>$icecream['img']
                 ]);
            }
    }
}
