<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImgColumnToIcecreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('icecreams', function (Blueprint $table) {
            $table->string('img')->after('description')->default('https://www.theflavorbender.com/wp-content/uploads/2019/06/Homemade-Vanilla-Ice-Cream-Featured1-200x200.jpg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('icecreams', function (Blueprint $table) {
            $table->dropColumn('img');
        });
    }
}
