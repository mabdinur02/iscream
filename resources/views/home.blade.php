<x-layout>



<header >
  <div class="container">
  <x-navbar></x-navbar>
   {{-- <div class="content">
       <h1 class="text-white font-weight-bold">Welcome on Iscream</h1>
       <h2   class="lead text-white">Find your favourite shops now and check their variety!</h2>
       
       <form action="{{route('shop.search')}}" method="GET">
       <div class="active-main-3">
        <input class="form-control" type="text" placeholder="Search" name="q" aria-label="Search">
        <button type="submit" class="btn btn-success">Search</button>
      </div>
      </form>
       <a href="{{ route('shop.create') }}">
       <div class="buttons">
        <button class="blob-btn">
          Add shop
          <span class="blob-btn__inner">
            <span class="blob-btn__blobs">
              <span class="blob-btn__blob"></span>
              <span class="blob-btn__blob"></span>
              <span class="blob-btn__blob"></span>
              <span class="blob-btn__blob"></span>
            </span>
          </span>
        </button>
        <br/>
      
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
          <filter id="goo">
            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"></feGaussianBlur>
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 21 -7" result="goo"></feColorMatrix>
            <feBlend in2="goo" in="SourceGraphic" result="mix"></feBlend>
          </filter>
        </defs>
      </svg>
   </div>
  </a>
--}}
{{-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="banner1 h-100"></div>
      <div class="carousel-caption">
        <h2>We love <span>Iscream</span></h2>
        <h3>Find your local shop now!</h3>
        <p><a href=""></a></p>
      </div>
    </div>
    <div class="item">
      <div class="banner2"></div>
      <div class="carousel-caption">
        <h2>Get your <span>Shop</span> online now!</h2>
        <h3>Add your flavours</h3>
        <p><a href=""></a></p>
      </div>
    </div>
    <div class="item">
      <div class="banner3"></div>
      <div class="carousel-caption">
        <h2>Straight to your door in <span>15 minutes</span>!</h2>
        <h3>Get it delivered in no time.</h3>
        <p><a href=""></a></p>
      </div>
    </div>
   
  </div>--}}
</div>
</header>
<section class="banner">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h1>Welcome on Iscream</h1>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vel ab voluptas nihil praesentium corrupti recusandae aut distinctio quod magnam eveniet. Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur reprehenderit corporis maiores accusamus.</p>
        <form class="d-flex mt-3" action="{{route('shop.search')}}" method="GET">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
        <a href="{{ route('shop.create') }}">
          <div class="buttons">
           <button class="blob-btn">
             Add shop
             <span class="blob-btn__inner">
               <span class="blob-btn__blobs">
                 <span class="blob-btn__blob"></span>
                 <span class="blob-btn__blob"></span>
                 <span class="blob-btn__blob"></span>
                 <span class="blob-btn__blob"></span>
               </span>
             </span>
           </button>
           <br/>
         
         <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
           <defs>
             <filter id="goo">
               <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"></feGaussianBlur>
               <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 21 -7" result="goo"></feColorMatrix>
               <feBlend in2="goo" in="SourceGraphic" result="mix"></feBlend>
             </filter>
           </defs>
         </svg>
      </div>
     </a>
      </div>
    </div>
  </div>
</section>
<div class="container mt-3">
    <div class="row">
       <div class="col 12">
            @if (session('status'))
            <div class="alert alert-success text-center">
                {{ session('status') }}
            </div>
            @endif
        </div> 

    </div>
</div>
<div class="container shop-cont">
  <div class="row">
             <div class="col-12">
              <h2 class="my-5 text-center home-shops">The latest shops added</h2>
            </div>
            @foreach ($shops as $shop)
            <div class="col-md-3">
              <!-- Card -->
              <div class="card shop-card my-3">
                
                <!-- Card image -->
                <div class="view overlay">
                  @if ($shop->img)
                  <img class="card-img-top" src="{{Storage::url($shop->img)}}" alt="Card image cap">
                  @else 
                  <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                  @endif
                  <a href="#!">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                
                <!-- Card content -->
                <div class="card-body">
                  
                  <!-- Title -->
                  <h4 class="card-title font-weight-bold"><a>{{$shop->name}}</a></h4>
                  <!-- Data -->
                  <ul class="list-unstyled list-inline rating mb-0">
                    <li class="list-inline-item mr-0"><i class="fas fa-star"> </i></li>
                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                    <li class="list-inline-item"><i class="fas fa-star-half-alt"></i></li>
                    <li class="list-inline-item"><p class="text-muted">4.5 (413)</p></li>
                  </ul>
                  <!-- Text -->
                  <p class="">{{$shop->description}}</p>
                  <hr class="my-4">
                  <p class="lead"><strong class="text-dark">Opening hours</strong></p>
                  <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                    <li class="list-inline-item mr-0">
                      <div class="chip mr-0">9:30-13:30</div>
                    </li>
                    <li class="list-inline-item mr-0">
                      <div class="chip mr-0">15:00-22:00</div>
                    </li>
                  </ul>
                  <!-- Button -->
                  {{-- <a href="{{route('shop.show', compact('shop'))}}" class="btn btn-custom d-flex justify-content-center btn-card mt-2">Details</a> --}}
                  <a href="{{route('shop.show', compact('shop'))}}" class="button">Details</a>
                </div>
              </div>
              <!-- Card -->
            </div>
            @endforeach
          </div>
        </div>
        
        {{-- <div class="container-fluid cont-why mt-5">
        <div class="container cont-why1">
          <div class="row"> 
            <div class="why">
                <h2 class="whyus">Why Choose us?</h2>
            </div>
          </div> 
            <div class="row">
                <div class="col-md-3 info text-center">
                    
                        <i class="far fa-clock  icon"></i>
                  
                    <h3><strong>Very fast</strong></h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, modi, qui quis tempore, in libero vero ut ipsum fugiat nisi reiciendis facere quod deleniti cupiditate. Rerum sapiente quasi corporis asperiores.</p>
                </div>
                <div class="col-md-3 info text-center">
                   
                        <i class="far fa-thumbs-up icon"></i>
                    
                    <h3><strong>Simply the best!</strong></h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, modi, qui quis tempore, in libero vero ut ipsum fugiat nisi reiciendis facere quod deleniti cupiditate. Rerum sapiente quasi corporis asperiores.</p>
                </div>
                <div class="col-md-3 info text-center">
                  
                        <i class="fas fa-network-wired icon"></i>
                   
                    <h3><strong>Biggest network</strong></h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, modi, qui quis tempore, in libero vero ut ipsum fugiat nisi reiciendis facere quod deleniti cupiditate. Rerum sapiente quasi corporis asperiores.</p>
                </div>
                <div class="col-md-3 info text-center">
                   
                        <i class="fas fa-ice-cream icon"></i>
               
                    <h3><strong>Top brands</strong></h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nam, modi, qui quis tempore, in libero vero ut ipsum fugiat nisi reiciendis facere quod deleniti cupiditate. Rerum sapiente quasi corporis asperiores.</p>
                </div>
            </div>
        </div>
        </div> --}}
      </x-layout>