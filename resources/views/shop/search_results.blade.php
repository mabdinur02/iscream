<x-layout>
  <div class="container"> <x-navbar></x-navbar> </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <h2>Your search results for: {{$q}}</h2>
            </div>
        </div>
        <div class="row mt-4">
            @foreach ($shops as $shop)
            <div class="col-md-3">
              <!-- Card -->
              <div class="card shop-card my-3">
                
                <!-- Card image -->
                <div class="view overlay">
                  @if ($shop->img)
                  <img class="card-img-top" src="{{Storage::url($shop->img)}}" alt="Card image cap">
                  @else 
                  <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                  @endif
                  <a href="#!">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                
                <!-- Card content -->
                <div class="card-body">
                  
                  <!-- Title -->
                  <h4 class="card-title font-weight-bold"><a>{{$shop->name}}</a></h4>
                  <!-- Data -->
                  <ul class="list-unstyled list-inline rating mb-0">
                    <li class="list-inline-item mr-0"><i class="fas fa-star"> </i></li>
                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                    <li class="list-inline-item"><i class="fas fa-star-half-alt"></i></li>
                    <li class="list-inline-item"><p class="text-muted">4.5 (413)</p></li>
                  </ul>
                  <!-- Text -->
                  <p class="">{{$shop->description}}</p>
                  <hr class="my-4">
                  <p class="lead"><strong class="text-main">Opening hours</strong></p>
                  <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                    <li class="list-inline-item mr-0">
                      <div class="chip mr-0">9:30-13:30</div>
                    </li>
                    <li class="list-inline-item mr-0">
                      <div class="chip mr-0">15:00-22:00</div>
                    </li>
                  </ul>
                  <!-- Button -->
                  {{-- <a href="{{route('shop.show', compact('shop'))}}" class="btn btn-custom d-flex justify-content-center btn-card mt-2">Details</a> --}}
                  <a href="{{route('shop.show', compact('shop'))}}" class="button">Details</a>
                </div>
              </div>
              <!-- Card -->
            </div>
            @endforeach

        </div>
    </div>
</x-layout>