<x-layout>
  <div class="container">
  <x-navbar></x-navbar>
</div>
  <div class="container contact-form">
    <div class="error">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
    <div class="contact-image">
      <img src="https://image.freepik.com/free-vector/ice-cream-icon-fast-food-collection-food-icon-isolated_138676-503.jpg" alt="rocket_contact"/>
    </div>
    <form method="post" action="{{route('shop.store')}}" enctype="multipart/form-data">
      @csrf
      <h3 class="text-sec">Add your shop</h3>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <p class="lead text-main">Name</p>
            <input type="text" class="form-control" name="name" value="{{old('name')}}">
          </div>
          <div class="form-group">
            <p class="lead text-main">Images</p>
            <input type="file" class="form-control" id="exampleCheck1" name="img">
          </div>
          <div class="form-group">
            <p class="lead text-main">Icecream selection</p>
            <select multiple name="selection[]" id="" class="form-control">
              @foreach ($icecreams as $icecream)
              <option value="{{$icecream->id}}">{{$icecream->name}}</option>
              @endforeach
              
            </select>
          </div>
          <div class="form-group">
            <input type="submit" name="btnSubmit" class="btnContact" value="Create" class="btn" />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <p class="lead text-main">Address</p>
            <input type="text" class="form-control" name="address" value="{{old('address')}}">
          </div>
          <div class="form-group">
            <p class="lead text-main">Description</p>
            <textarea name="description" class="form-control" id="" cols="30" rows="10">{{old('description')}}</textarea>
          </div>
        </div>
      </div>
    </form>
  </div>
</x-layout>