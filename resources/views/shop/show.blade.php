<x-layout>
  <div class="container">
  <x-navbar></x-navbar>
</div>
{{-- <div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-title">
                <h3>{{$shop->name}}</h3>
                </div>
                <div class="card-body">
                <p class="lead">{{$shop->description}}</p>
                @if ($shop->icecreams)
                @foreach ($shop->icecreams as $icecream)
                <p>{{$icecream->name}}</p>
                <p>{{$icecream->brand}}</p>
                <p>{{$icecream->description}}</p>
                <img src="{{$icecream->img}}" alt="">   
                @endforeach
                @endif
                </div>
                <div class="card-img">
                <img src="{{Storage::url($shop->img)}}" alt="">
                </div>
            </div>
        <a href="{{route('home')}}" class="btn btn-info">Return home</a>
        c
        <form action="{{route('shop.destroy', compact('shop'))}}" method="POST">
            @method('delete')
            @csrf
       <button class="btn btn-danger"> <span> Delete your post</span></button>
        </form>
        </div>
    </div>
</div> --}}
<div class="container show-cont">
    <div class="row">
        <div class="col-md-7">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block img-fluid" src="{{Storage::url($shop->img)}}" alt="First slide">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="{{Storage::url($shop->img)}}" alt="Second slide">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="{{Storage::url($shop->img)}}" alt="Third slide">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
        </div>
        <div class="col-md-5">
            <h2 class="text-white">{{$shop->name}}</h2>
            <ul class="list-unstyled list-inline rating mb-0">
                <li class="list-inline-item mr-0"><i class="fas fa-star"> </i></li>
                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                <li class="list-inline-item"><i class="fas fa-star-half-alt"></i></li>
                <li class="list-inline-item"><p class="text-muted text-white">4.5 (413)</p></li>
              </ul>
            <p class="text-white"><b class="text-sec">Description</b><br>{{$shop->description}}</p>
            <p class="text-white"><b class="text-sec">Address</b><br>{{$shop->address}}</p>
            <p class="text-white"><b class="text-sec">Opening hours</b></p>
            <p class="times1 text-white">9.30-13.00</p> <p class="times2 text-white">15.00-22.00</p>
            <div class="row">
                <div class="col-md-4">
                    <a href="{{route('shop.edit',compact('shop'))}}" class="btn btn-success">Edit shop</a>
                </div>
                <div class="col-md-4"> <a href="{{route('home')}}" class="btn btn-secondary">Return home</a></div>
                <div class="col-md-4"><form action="{{route('shop.destroy', compact('shop'))}}" method="POST">
                    @method('delete')
                    @csrf
               <button class="btn btn-danger"> <span> Delete shop</span></button>
                </form></div>
                @if (Auth::user()->shop)
            @endif
            </div>
           
        </div>
    </div>
    <div class="row my-4">
        <div class="col-12">
            <h3 class="text-center text-white">Icecream selection</h3>
        </div>
        
        @foreach ($shop->icecreams as $icecream)
        <div class="col-md-3 overflow mb-3">
            <div class="icecream-card h-100">
                <img class="card-img" src="{{$icecream->img}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="text-sec">{{$icecream->name}}</h5>
                  <p class="small italic">{{$icecream->brand}}</p>
                  <hr>
                  <p class="">{{$icecream->description}}</p>
                  <button class="btn btn-success float-right mb-1">Add to cart</button>
                </div>
              </div>
        </div>
        @endforeach
   
    </div>
</div>
</x-layout>