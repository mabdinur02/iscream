<footer class="page-footer bg-dark">
    <div class="bg-sec">
        <div class="container">
            <div class="row py-4 d-flex align-items-center">
                <div class="col-md-12 text-center">
                    <a href=""><i class="fab fa-facebook-f text-white mr-4"></i></a>
                    <a href=""><i class="fab fa-twitter text-white mr-4"></i></a>
                    <a href=""><i class="fab fa-google-plus-g text-white mr-4"></i></a>
                    <a href=""><i class="fab fa-linkedin-in text-white mr-4"></i></a>
                    <a href=""><i class="fab fa-instagram text-white"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container text-center text-white text-md-left mt-5">
        <div class="row">
           <div class="col-md-3 mx-auto mb-4">
            <h6 class="text-uppercase font-weight-bold">Iscream</h6>
            <hr class="bg-sec mb-4 mt-0 d-inline-block mx-auto" style="width: 65px; height: 2px">
            <p class="mt-2">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde, autem quisquam! Repellendus obcaecati numquam esse autem quia totam facere aperiam, libero magni optio illo at, quam omnis tempora, eaque amet!</p>
           </div> 
           <div class="col-md-2 mx-auto mb-4">
            <h6 class="text-uppercase font-weight-bold">Products</h6>
            <hr class="bg-sec mb-4 mt-0 d-inline-block mx-auto" style="width: 85px; height: 2px">
            <ul class="list-unstyled">
                <li class="my-2"><a href="">HTML</a></li>
                <li class="my-2"><a href="">CSS 3</a></li>
                <li class="my-2"><a href="">Bootstrap 4</a></li>
                <li class="my-2"><a href="">Javascript</a></li>
            </ul>
           </div> 
           <div class="col-md-2 mx-auto mb-4">
            <h6 class="text-uppercase font-weight-bold">Useful Links</h6>
            <hr class="bg-sec mb-4 mt-0 d-inline-block mx-auto" style="width: 110px; height: 2px">
            <ul class="list-unstyled">
                <li class="my-2"><a href="">Home</a></li>
                <li class="my-2"><a href="">About us</a></li>
                <li class="my-2"><a href="">Services</a></li>
                <li class="my-2"><a href="">Contact</a></li>
            </ul>
           </div> 
           <div class="col-md-3 mx-auto mb-4">
            <h6 class="text-uppercase font-weight-bold">Contact</h6>
            <hr class="bg-sec mb-4 mt-0 d-inline-block mx-auto" style="width: 75px; height: 2px">
            <ul class="list-unstyled">
                <li class="my-2"><i class="fas fa-home mr-1"></i>Karachi , street 3 PK</li>
                <li class="my-2"><i class="far fa-envelope mr-1"></i>theproviders@gmail.com</li>
                <li class="my-2"><i class="fas fa-phone mr-1"></i> + 39 0552427457</li>
                <li class="my-2"><i class="fas fa-print mr-1"></i> + 39 0544582727</li> 
            </ul>
           </div> 
        </div>
    </div>

</footer>