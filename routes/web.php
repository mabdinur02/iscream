<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/create', [ShopController::class, 'create'])->name('shop.create');
Route::post('/store', [ShopController::class, 'store'])->name('shop.store');
Route::get('/thankyou', [ShopController::class, 'thankyou'])->name('thankyou');
Route::get('/show/{shop}', [ShopController::class, 'show'])->name('shop.show');
Route::get('/edit/{shop}', [ShopController::class, 'edit'])->name('shop.edit');
Route::put('/update/{shop}', [ShopController::class, 'update'])->name('shop.update');
Route::delete('/destroy/{shop}', [ShopController::class, 'destroy'])->name('shop.destroy');
Route::get('/search', [ShopController::class, 'search'])->name('shop.search');








